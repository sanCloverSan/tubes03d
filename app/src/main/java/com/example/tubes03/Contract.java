package com.example.tubes03;

import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountryData;
import com.example.tubes03.model.CountrySummary;
import com.example.tubes03.model.SummaryData;

import java.util.Date;
import java.util.List;

/*
 * Reference:
 * https://www.journaldev.com/14886/android-mvp
 * https://github.com/kiterunner20/TruckeeApp-
 * */
public class Contract {
    interface Model {
        interface OnFinishedListener {
            void onFailure(View view, Throwable t);

            void onResponseSuccess(View view, List<CountryData> countryDataList, int viewPart);

            void onResponseSuccess(View view, SummaryData summaryData, int viewPart);

            void onResponseSuccess0(View view, List<Country> countries, int viewPart);

        }

        void getGlobalSummaryData(View view, OnFinishedListener onFinishedListener, int viewPart);

        void getCountryAllStatus(View view, OnFinishedListener onFinishedListener, String countrySlug, int viewPart);

        void getCountries(View view, OnFinishedListener onFinishedListener, int viewPart);
    }

    interface View {
        void showProgress();

        void hideProgress();

        void onResponseFailure(Throwable throwable);

        void setEmptyData();
    }

    interface GlobalView extends View {
        void getGlobalSummaryData(int viewPart);

        void onResponseSuccess(SummaryData response, int viewPart);
    }

    interface CountryView extends View {
        void getCountries(int viewPart);

        void getCountryAllStatus(String countrySlug);

        void getCountryAllStatus(String countrySlug, Date from, Date to);

        void onResponseSuccess(List<CountryData> list, int viewPart);

        void onResponseSuccess(List<Country> list);

    }

    interface Presenter {
        void onDestroy();

        void getCountries(View view, int viewPart);

        void getGlobalSummaryData(View view, int viewPart);

        void getCountryAllStatus(View view, String countrySlug, int viewPart);
    }
}
