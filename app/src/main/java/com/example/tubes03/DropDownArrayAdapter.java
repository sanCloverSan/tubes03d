package com.example.tubes03;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tubes03.model.Country;
import com.example.tubes03.model.Protocol;

import java.util.ArrayList;
import java.util.List;

import gr.escsoft.michaelprimez.searchablespinner.interfaces.ISpinnerSelectedView;

public class DropDownArrayAdapter extends ArrayAdapter<Country> implements Filterable, ISpinnerSelectedView {
    private Context mContext;
    private ArrayList<Country> mBackupCountry;
    private ArrayList<Country> mCountry;
    private CountryFilter mCountryFilter = new CountryFilter();

    public DropDownArrayAdapter(Context context, List<Country> list) {
        super(context, R.layout.dropdown_item);
        this.mContext = context;
        this.mCountry = new ArrayList<>(list);
        this.mBackupCountry = new ArrayList<>(list);

    }


    @Override
    public int getCount() {
        return mCountry == null ? 0 : mCountry.size() + 1;
    }

    @Nullable
    @Override
    public Country getItem(int position) {
        View view = null;
        if (this.mCountry.isEmpty() || position <= 0) return null;
        return this.mCountry.get(position-1);
    }

    public void setCountryList(List<Country> list) {
        this.mCountry = new ArrayList<>(list);
        this.mBackupCountry = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (position == 0) {
            view = getNoSelectionView();
        } else {
            view = View.inflate(mContext, R.layout.dropdown_item, null);
            TextView displayName = (TextView) view.findViewById(R.id.tv_dropdown_item);
            displayName.setText(mCountry.get(position - 1).getCountry());
        }
        return view;
    }


    @Override
    public View getNoSelectionView() {
        View view = View.inflate(mContext, R.layout.dropdown_view_no_selection, null);
        return view;
    }

    @Override
    public View getSelectedView(int position) {
        View view = null;
        if (position == 0) {
            view = getNoSelectionView();
        } else {
            view = View.inflate(mContext, R.layout.dropdown_selected_item, null);
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.updateView(getItem(position), position);
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return mCountryFilter;
    }

    public void setmBackupCountry(ArrayList<Country> mBackupCountry) {
        this.mBackupCountry = mBackupCountry;
        this.mCountry = mBackupCountry;
        this.notifyDataSetChanged();
    }

    public class CountryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults filterResults = new FilterResults();

            if (TextUtils.isEmpty(constraint)) {
                filterResults.count = mBackupCountry.size();
                filterResults.values = mBackupCountry;
                return filterResults;
            }

            final ArrayList<Country> filterCountry = new ArrayList<>();

            for (Country countryData : mBackupCountry) {
                if (countryData.getCountry().toLowerCase().contains(constraint)) {
                    filterCountry.add(countryData);
                }
            }

            filterResults.count = filterCountry.size();
            filterResults.values = filterCountry;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mCountry = (ArrayList) results.values;
            notifyDataSetChanged();
        }
    }

    public enum ItemViewType {
        ITEM, NO_SELECTION_ITEM;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView tvText;

        public ViewHolder(View view) {
            super(view);
            this.tvText = view.findViewById(R.id.tv_dropdown_selected_item);
        }

        public void updateView(Country country, int position) {
            this.position = position;
            if(country !=  null){
                this.tvText.setText(country.getCountry());
            }
        }
    }
}