package com.example.tubes03.model;

public class Protocol {
    protected String title;
    protected String text;
    public int idIcon;

    public Protocol(String title, String text, int idIcon){
        this.title = title;
        this.text = text;
        this.idIcon = idIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getIdIcon() {
        return idIcon;
    }

    public void setIdIcon(int idIcon) {
        this.idIcon = idIcon;
    }
}
