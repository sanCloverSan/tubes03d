package com.example.tubes03.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummaryData {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Global")
    @Expose
    private GlobalData global;
    @SerializedName("Countries")
    @Expose
    private List<CountrySummary> countries = null;

    public SummaryData(){
        this.message = "";
        this.countries = new ArrayList<>();
        this.global = new GlobalData();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GlobalData getGlobal() {
        return global;
    }

    public void setGlobal(GlobalData global) {
        this.global = global;
    }

    public List<CountrySummary> getCountries() {
        return countries;
    }

    public void setCountries(List<CountrySummary> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return "SummaryData{" +
                "message='" + message + '\'' +
                ", global=" + global +
                ", countries=" + countries +
                '}';
    }
}