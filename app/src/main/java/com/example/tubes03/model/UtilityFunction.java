package com.example.tubes03.model;

public class UtilityFunction {


    /**
     * Method untuk mengubah angka menjadi string dengan pemisahan menggunakan tanda koma per tiga digit dimulai dari akhir
     *
     * @param number
     * @return
     */
    public static String formatNumberToString(int number) {
        String result = "";
        String str = number + "";
        if (str.length() > 0) {
            int digitCounter = 0;
            for (int i = str.length() - 1; i >= 0; i--) {
                if (i != str.length() - 1 && digitCounter % 3 == 0) {
                    result = "," + result;
                }

                if (str.charAt(i) != '.') {
                    digitCounter += 1;
                }

                result = str.charAt(i) + result;
            }
        }
        return result;
    }
}
