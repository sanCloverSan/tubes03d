package com.example.tubes03.model;

import java.util.List;

public interface FragmentListener {
    public SummaryData getGlobalSummary();
    public List<CountryData> getCountrySummary(String countrySlug);
}
