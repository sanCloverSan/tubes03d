package com.example.tubes03;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tubes03.model.Protocol;

import org.w3c.dom.Text;

import java.util.LinkedList;
import java.util.List;


public class ProtocolAdapter extends RecyclerView.Adapter<ProtocolAdapter.ViewHolder> {
    protected List<Protocol> listProtocol;
    private Context context;
    private Presenter presenter;

    public ProtocolAdapter(Context context, Presenter presenter, List<Protocol> listProtocol){
        this.context = context;
        this.presenter = presenter;
        this.listProtocol = listProtocol;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View convertView = layoutInflater.inflate(R.layout.protocol_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        convertView.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.updateView(this.listProtocol.get(position), position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return this.listProtocol.size();
    }


     class ViewHolder extends RecyclerView.ViewHolder {
        private int position;
        private TextView title;
        private TextView tvText;
        private ImageView icon;

        public ViewHolder(View view) {
            super(view);
            this.title = view.findViewById(R.id.tv_title);
            this.tvText = view.findViewById(R.id.tv_text);
            this.icon = view.findViewById(R.id.iv_icon);
        }

        public void updateView(Protocol protocol, int position) {
            this.position = position;
            this.title.setText(protocol.getTitle());
            this.tvText.setText(protocol.getText());
            this.icon.setImageResource(protocol.idIcon);
            this.icon.invalidate();
        }
    }
}
