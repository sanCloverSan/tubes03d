package com.example.tubes03;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountrySummary;
import com.example.tubes03.network.APIClient;
import com.example.tubes03.model.CountryData;
import com.example.tubes03.model.SummaryData;
import com.example.tubes03.network.APIInterface;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class Presenter implements Contract.Presenter, Contract.Model.OnFinishedListener {
    private Context context;

    private APIInterface apiInterface;
    private List<CountryData> countryDataList;
    private SummaryData globalSummary;

    private Contract.Model requestModel;

    private Country selectedCountry;

    private CovidTrackerPreference covidTrackerPreference;

    public Presenter(Context context) {
        this.context = context;
        this.apiInterface = APIClient.getClient().create(APIInterface.class);
        this.countryDataList = new ArrayList<>();
        this.globalSummary = new SummaryData();
        this.requestModel = new RequestModel(this.apiInterface);
        this.covidTrackerPreference = new CovidTrackerPreference(context);

        if(this.getSavedCountryFromPreference() == null && this.getCountrySummaryFromPreference() == null){
            Country country = new Country();
            country.setCountry("Indonesia");
            country.setSlug("Indonesia");
            country.setISO2("ID");

            CountrySummary countrySummary = new CountrySummary();
            Date currentTime = Calendar.getInstance().getTime();
            String stringDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ").format(currentTime);
            countrySummary.setDate(stringDate);

            this.saveSelectedCountryToPreference(country, countrySummary);
        }
    }


    public List<CountryData> getCountryDataList() {
        return countryDataList;
    }

    public SummaryData getSummaryDataObj() {
        return this.globalSummary;
    }

    public void setSelectedCountry(Country selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public Country getSelectedCountry() {
        return selectedCountry;
    }

    /**
     * Method untuk mengambil objek summary data global dan summary per negaranya
     *
     * @return objek
     */
    public SummaryData getGlobalSummary() {
        return globalSummary;
    }

    public CountrySummary getCountrySummary(String countrySlug){
        CountrySummary summary = null;

        if(this.globalSummary != null && this.globalSummary.getCountries() != null){

            Iterator<CountrySummary> iterator = this.globalSummary.getCountries().iterator();

            while(iterator.hasNext()){
                CountrySummary temp = iterator.next();

                if(temp.getSlug().equals(countrySlug)){
                    summary = temp;
                    break;
                }
            }
        }

        return summary;
    }


    @Override
    public void onFailure(Contract.View view, Throwable t) {
        //Menghilangkan loading screen
        if (view != null) {
            view.hideProgress();
        }
        view.onResponseFailure(t);
    }

    @Override
    public void onResponseSuccess(Contract.View view, List<CountryData> countryDataList, int viewPart) {
        if (view != null) {
            view.hideProgress();
        }
        if (view instanceof Contract.CountryView) {
            Contract.CountryView countryView = (Contract.CountryView) view;
            countryView.onResponseSuccess(countryDataList, viewPart);
        }
    }

    @Override
    public void onResponseSuccess0(Contract.View view, List<Country> countries, int viewPart) {
        if (view != null) {
            view.hideProgress();
        }
        if (view instanceof Contract.CountryView) {
            Contract.CountryView countryView = (Contract.CountryView) view;
            countryView.onResponseSuccess(countries);
        }
    }

    @Override
    public void onResponseSuccess(Contract.View view, SummaryData summaryData, int viewPart) {
        if (view != null) {
            view.hideProgress();
        }
        if (view instanceof Contract.GlobalView) {
            Contract.GlobalView countryView = (Contract.GlobalView) view;
            countryView.onResponseSuccess(summaryData, viewPart);
        }
    }


    public void setCountryDataList(List<CountryData> countryDataList) {
        this.countryDataList = countryDataList;
    }

    public void setGlobalSummary(SummaryData summaryData) {
        this.globalSummary = summaryData;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void getCountries(Contract.View view, int viewPart) {
        //memunculkan loading screen
        if (view != null) {
            view.showProgress();
        }
        //Memulai request
        requestModel.getCountries(view, this, viewPart);
    }

    @Override
    public void getGlobalSummaryData(Contract.View view, int viewPart) {
        //memunculkan loading screen
        if (view != null) {
            view.showProgress();
        }
        //Memulai request
        requestModel.getGlobalSummaryData(view, this, viewPart);
    }

    @Override
    public void getCountryAllStatus(Contract.View view, String countrySlug, int viewPart) {
        //memunculkan loading screen
        if (view != null) {
            view.showProgress();
        }
        requestModel.getCountryAllStatus(view, this, countrySlug, viewPart);
    }

    public void saveSelectedCountry(Country country, CountrySummary countrySummary){
        this.covidTrackerPreference.saveSelectedCountry(country, countrySummary);
    }

    public void saveSelectedCountryToPreference(Country country, CountrySummary countrySummary){
        this.covidTrackerPreference.saveSelectedCountry(country, countrySummary);
    }
    public Country getSavedCountryFromPreference(){
        return this.covidTrackerPreference.getSavedCountry();
    }

    public CountrySummary getCountrySummaryFromPreference(){
        return this.covidTrackerPreference.getSavedCountrySummary();
    }

}
