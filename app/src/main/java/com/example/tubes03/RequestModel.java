package com.example.tubes03;

import android.util.Log;

import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountryData;
import com.example.tubes03.model.SummaryData;
import com.example.tubes03.network.APIInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/*
 * Class modeling:
 * -    https://www.journaldev.com/14886/android-mvp
 * -    https://github.com/kiterunner20/TruckeeApp-

 * Retrofit References:
 * -   https://androidpedia.net/en/tutorial/1132/retrofit2
 * -   https://www.youtube.com/watch?v=1FVoBM9t2Ik
 * */
public class RequestModel implements Contract.Model {
    private APIInterface apiInterface;

    public RequestModel(APIInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    /*
        ###############################################################################
        METHODS FOR CALLING REQUEST
        ###############################################################################
         */

    /**
     * Method untuk fetch summary data global dan summary per negaranya
     *
     * @return objek
     */
    @Override
    public void getGlobalSummaryData(Contract.View view, OnFinishedListener onFinishedListener, int viewPart) {
        //Panggil fungsi yang ada inAPIInterface untuk mengambil
        Call<SummaryData> call = this.apiInterface.getGlobalSummary();
        call.enqueue(new Callback<SummaryData>() {
            @Override
            public void onResponse(Call<SummaryData> call, Response<SummaryData> response) {
//                Log.d("debug request", "onResponse Global : " + response.body().toString());
                if (response.body() != null) {
                    if (response.body().getGlobal() != null) {
                        Log.d("debug request", "onResponse Global : " + response.body().toString());
                    }
                    onFinishedListener.onResponseSuccess(view, response.body(), viewPart);
                }
            }

            @Override
            public void onFailure(Call<SummaryData> call, Throwable t) {
                Log.d("debug request", "onFailure Global : " + t.toString());
                onFinishedListener.onFailure(view, t);
            }
        });
    }

    /**
     * Method untuk fetch data perkembangan berdasarkan param negara
     *
     * @param countrySlug slug negara sesuai dengan yang tersedia pada API
     * @return
     */
    @Override
    public void getCountryAllStatus(Contract.View view, OnFinishedListener onFinishedListener, String countrySlug, int viewPart) {
        Call<List<CountryData>> call = this.apiInterface.getCountrySummary(countrySlug);
        call.enqueue(new Callback<List<CountryData>>() {
            @Override
            public void onResponse(Call<List<CountryData>> call, Response<List<CountryData>> response) {
                if (response.body() != null) {
                    Log.d("debug request", "onResponse Country: " + response.body().toString());
                    onFinishedListener.onResponseSuccess(view, response.body(), viewPart);
                }
            }

            @Override
            public void onFailure(Call<List<CountryData>> call, Throwable t) {
                Log.d("debug request", "onFailure Country: " + t.getLocalizedMessage());
                onFinishedListener.onFailure(view, t);
            }
        });
    }

    /**
     * Method untuk mengambil list negara dari API
     * @param view view tempat request ini dipanggil
     * @param onFinishedListener listener yang akan mengatasi response dari request
     * @param viewPart bagian dari view yang harus di update menggunakan hasil dari request
     */
    @Override
    public void getCountries(Contract.View view, OnFinishedListener onFinishedListener, int viewPart) {
        Call<List<Country>> call = this.apiInterface.getCountries();
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.body() != null) {
                    Log.d("debug request", "onResponse Country: " + response.body().toString());
                    onFinishedListener.onResponseSuccess0(view, response.body(), viewPart);
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.d("debug request", "onFailure Country: " + t.getLocalizedMessage());
                onFinishedListener.onFailure(view, t);
            }
        });
    }


}
