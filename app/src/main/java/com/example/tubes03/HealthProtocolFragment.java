package com.example.tubes03;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tubes03.databinding.FragmentInformationBinding;
import com.example.tubes03.databinding.FragmentProtocolBinding;

public class HealthProtocolFragment extends Fragment {

    private ProtocolAdapter adapter;
    private RecyclerView rvProtocol;
    private static HealthProtocolFragment healthProtocolFragment;

    public static HealthProtocolFragment newInstance(ProtocolAdapter adapter){
        healthProtocolFragment = new HealthProtocolFragment();
        healthProtocolFragment.adapter = adapter;
        return healthProtocolFragment;
    }

    public HealthProtocolFragment(){

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentProtocolBinding binding = FragmentProtocolBinding.inflate(getLayoutInflater());
        this.rvProtocol = binding.listText;

        this.rvProtocol.setHasFixedSize(true);
        this.rvProtocol.setLayoutManager(new LinearLayoutManager(this.getContext()));
        this.rvProtocol.setAdapter(this.adapter);
        return binding.getRoot();

    }
}
