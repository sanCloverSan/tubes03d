package com.example.tubes03;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.NestedScrollingChild;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;

import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.example.tubes03.databinding.FragmentHomeBinding;
import com.example.tubes03.model.GlobalData;
import com.example.tubes03.model.SummaryData;
import com.example.tubes03.model.UtilityFunction;

import java.util.ArrayList;
import java.util.List;

import static com.example.tubes03.model.UtilityFunction.formatNumberToString;

public class HomeFragment extends Fragment implements Contract.GlobalView, SwipeRefreshLayout.OnRefreshListener {
    private final static int PIE_CHART_PART = 0;
    private final static int CARD_VIEWS_PART = 1;
    private final static int TOP_HIGHEST_CASE_PART = 2;

    private Context context;
    private Presenter presenter;
    private TextView tvAngkaKasusBaruTerkonfirmasi;
    private TextView tvAngkaTotalKematian;
    private TextView tvAngkaKematianBaru;
    private TextView tvAngkaTotalTerkonfirmasi;
    private TextView tvAngkaTotalKesembuhan;
    private TextView tvAngkaKesembuhanBaru;
    private TextView tvRequestMessage;
    private TextView tvRequestDate;
    private SwipeRefreshLayout swipeRefreshLayoutGlobal;
    private NestedScrollView nsv_home;
    private LinearLayout llHomeLoadingScreen;
    private AnyChartView pieChartView;
    private View cachedView;
    private Pie pieChart;

    public static HomeFragment newInstance(Context context, Presenter presenter) {
        HomeFragment fragment = new HomeFragment();
        fragment.context = context;
        fragment.presenter = presenter;
        return fragment;
    }

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("debug home frg", "call on onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if(this.cachedView == null) {
            FragmentHomeBinding binding = FragmentHomeBinding.inflate(getLayoutInflater());
            this.llHomeLoadingScreen = binding.llHomeLoadingScreen;
            this.tvRequestMessage = binding.tvRequestMessage;
            this.tvRequestDate = binding.tvRequestDate;
            this.swipeRefreshLayoutGlobal = binding.swipeRefreshGlobal;
            this.swipeRefreshLayoutGlobal.setOnRefreshListener(this);
            this.nsv_home = binding.scrollViewHome;
            this.tvAngkaKasusBaruTerkonfirmasi = binding.tvAngkaKasusBaruTerkonfirmasi;
            this.tvAngkaTotalKematian = binding.tvAngkaTotalKematian;
            this.tvAngkaKematianBaru = binding.tvAngkaKematianBaru;
            this.tvAngkaTotalTerkonfirmasi = binding.tvAngkaTotalTerkonfirmasi;
            this.tvAngkaTotalKesembuhan = binding.tvAngkaTotalKesembuhan;
            this.tvAngkaKesembuhanBaru = binding.tvAngkaKesembuhanBaru;

            this.pieChartView = binding.pieChartGlobal;

            //Request untuk mengambil data summary global dari API
            this.getGlobalSummaryData(PIE_CHART_PART);
            this.cachedView = binding.getRoot();
        }

        Log.d("debug home frg", "call on onCreateView");

        return cachedView;
    }

    /**
     * Method untuk memunculkan loading screen
     */
    @Override
    public void showProgress() {
        this.tvRequestMessage.setVisibility(View.GONE);
        this.llHomeLoadingScreen.setVisibility(View.VISIBLE);
        this.nsv_home.setVisibility(View.GONE);
    }

    /**
     * Method untuk menghilangkan loading screen
     */
    @Override
    public void hideProgress() {
        this.llHomeLoadingScreen.setVisibility(View.GONE);
        this.nsv_home.setVisibility(View.VISIBLE);
    }

    /**
     * Method untuk menampilkan error ketika request gagal
     *
     * @param throwable
     */
    @Override
    public void onResponseFailure(Throwable throwable) {
        this.tvRequestMessage.setVisibility(View.VISIBLE);
        this.tvRequestMessage.setText(throwable.getLocalizedMessage());
        this.setEmptyData();
    }

    /**
     * Method untuk mengisi tampilan apabila request gagal atau tidak mengembalikan data
     */
    @Override
    public void setEmptyData() {
        this.tvAngkaKasusBaruTerkonfirmasi.setText("No Data");
        this.tvAngkaTotalKematian.setText("No Data");
        this.tvAngkaKematianBaru.setText("No Data");
        this.tvAngkaTotalTerkonfirmasi.setText("No Data");
        this.tvAngkaTotalKesembuhan.setText("No Data");
        this.tvAngkaKesembuhanBaru.setText("No Data");
    }

    @Override
    public void getGlobalSummaryData(int viewPart) {
        Handler handler = new Handler();
        Contract.View view = this;

        final Runnable r = new Runnable() {
            public void run() {
                presenter.getGlobalSummaryData(view, viewPart);
            }
        };

        handler.postDelayed(r, 500);

    }

    /**
     * Method untuk mengisi tampilan apabila request berhasil
     */
    @Override
    public void onResponseSuccess(SummaryData response, int viewPart) {
        this.presenter.setGlobalSummary(response);
        if (viewPart == PIE_CHART_PART || viewPart == CARD_VIEWS_PART) {
            //Update area card views
            this.setCardViewsPart(response);

            //Update area pie chart dan
            if (response != null && response.getGlobal() != null) {
                this.createPieChart(response.getGlobal());
            }

        }
    }


    /**
     * Method untuk melakukan manipulasi data hasil request ke area CardView
     *
     * @param summaryData
     */
    public void setCardViewsPart(SummaryData summaryData) {
        GlobalData globalData = summaryData.getGlobal();
        //Menampilkan pesan jika request berhasil, namun tidak ada data kembalian (Caching Request)
        if (!summaryData.getMessage().equals("")) {
            this.tvRequestMessage.setVisibility(View.VISIBLE);
            this.tvRequestMessage.setText(summaryData.getMessage());
        }

        //Set tanggal data terakhir di-update
        //mengambil bagian negara (Countries), karena pada data global tidak tersedia
        if (summaryData.getCountries() != null) {
            if (summaryData.getCountries().size() > 0) {
                String date = summaryData.getCountries().get(0).getDate();
                this.tvRequestDate.setText(date.split("T")[0]);
            }
        }

        if (globalData != null) {
            this.tvAngkaKasusBaruTerkonfirmasi.setText(formatNumberToString(globalData.getNewConfirmed()));
            this.tvAngkaTotalKematian.setText(formatNumberToString(globalData.getTotalDeaths()));
            this.tvAngkaKematianBaru.setText(formatNumberToString(globalData.getNewDeaths()));
            this.tvAngkaTotalTerkonfirmasi.setText(formatNumberToString(globalData.getTotalConfirmed()));
            this.tvAngkaTotalKesembuhan.setText(formatNumberToString(globalData.getTotalRecovered()));
            this.tvAngkaKesembuhanBaru.setText(formatNumberToString(globalData.getNewRecovered()));
        } else {
            this.setEmptyData();
        }
    }

    /**
     * Method untuk membuat Pie Chart data summary global
     *
     * @param globalData Data summary global hasil fetch dari API
     */
    public void createPieChart(GlobalData globalData) {
        /*
         * Reference:
         * https://github.com/AnyChart/AnyChart-Android/blob/master/sample/src/main/java/com/anychart/sample/charts/PieChartActivity.java
         * https://www.anychart.com/technical-integrations/samples/android-charts/
         * https://github.com/AnyChart/AnyChart-Android
         */

        this.pieChartView.setVisibility(View.VISIBLE);
        boolean prevNull = false;
        if(this.pieChart == null){
            this.pieChart = AnyChart.pie();
            prevNull = true;
        }

        //Data untuk membuat pie chart
        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("Terkonfirmasi Baru", globalData.getNewConfirmed()));
        data.add(new ValueDataEntry("Total Terkonfirmasi", globalData.getTotalConfirmed()));
        data.add(new ValueDataEntry("Terpulihkan Baru", globalData.getNewRecovered()));
        data.add(new ValueDataEntry("Total Terpulihkan", globalData.getTotalRecovered()));
        data.add(new ValueDataEntry("Kematian Baru", globalData.getNewDeaths()));
        data.add(new ValueDataEntry("Total Kematian", globalData.getTotalDeaths()));

        //Set data untuk chart
        this.pieChart.data(data);

        //Explode chart agar mudah terbaca
        Number[] arrIndexExplode = {0, 1, 2, 3, 4, 5};
        this.pieChart.select(arrIndexExplode);

        //Set judul dari chart
        this.pieChart.title("Data Kasus Covid-19 di Seluruh Dunia");

        //Set agar angka persentase untuk tiap potongan berada di luar
        this.pieChart.labels().position("outside");

        //Set background chart mengikuti warna background fragment
        this.pieChart.background().fill("#16213E");

        //Set label, garis, dan legenda agar berwarna putih
        this.pieChart.stroke("2 #FFFFFF");
        this.pieChart.legend().fontColor("#FFFFFF");
        this.pieChart.labels().fontColor("#FFFFFF");

        //Mengatur posisi dari legenda
        this.pieChart.legend()
                .position("center-bottom")
                .itemsLayout(LegendLayout.HORIZONTAL)
                .align(Align.CENTER);

        if(prevNull) {
            this.pieChartView.setChart(this.pieChart);
        }
    }

    @Override
    public void onRefresh() {
        Handler handler = new Handler();
        Contract.View view = this;

        final Runnable r = new Runnable() {
            public void run() {
                getGlobalSummaryData(PIE_CHART_PART);
            }
        };

        handler.postDelayed(r, 1000);
        this.swipeRefreshLayoutGlobal.setRefreshing(false);
    }
}
