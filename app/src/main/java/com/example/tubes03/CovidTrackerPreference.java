package com.example.tubes03;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountrySummary;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CovidTrackerPreference {
    protected SharedPreferences sharedPref;

    protected final static String NAMA_SHARED_PREF = "sp_covid_tracker";
    protected String COUNTRY_SUMMARY = "COUNTRY_SUMMARY";
    protected String COUNTRY = "COUNTRY";

    public CovidTrackerPreference(Context context) {
        this.sharedPref = context.getSharedPreferences(NAMA_SHARED_PREF, context.MODE_PRIVATE);
    }

    public void saveSelectedCountry(Country country, CountrySummary countrySummary) {
        if (countrySummary != null) {
            SharedPreferences.Editor editor = this.sharedPref.edit();
            Gson gson = new Gson();

            String jsonCountry = gson.toJson(country);
            editor.putString(COUNTRY, jsonCountry);

            String jsonCountrySummary = gson.toJson(countrySummary);
            editor.putString(COUNTRY_SUMMARY, jsonCountrySummary);

            editor.commit();
        }
    }

    public Country getSavedCountry() {
        Country country = null;

        Gson gson = new Gson();
        String stringCountry = this.sharedPref.getString(COUNTRY, "");
        if (stringCountry != "") {
            country = gson.fromJson(stringCountry, Country.class);
        }


        return country;
    }

    public CountrySummary getSavedCountrySummary() {
        CountrySummary countrySummary = null;
        Gson gson = new Gson();
        String stringCountrySummary = this.sharedPref.getString(COUNTRY_SUMMARY, "");

        if (stringCountrySummary != "") {
            countrySummary = gson.fromJson(stringCountrySummary, CountrySummary.class);
        }

        return countrySummary;
    }
}
