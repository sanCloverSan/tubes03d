package com.example.tubes03;

import com.example.tubes03.model.Protocol;

public class ProtocolData {
    public static Protocol[] protocols ={
        new Protocol("Menggunakan Masker","Ketika berpergian, selalu gunakan masker untuk menyaring udara yang kita hirup. Gantilah masker setiap hari",
                R.drawable.ic_mask_foreground),
        new Protocol("Selalu Mencuci Tangan", "Selalu mencuci tangan selama 20 detik setelah menyentuh apapun agar tetap steril.",
                R.drawable.ic_hand_wash),
        new Protocol("Jangan Menyentuh Wajah", "Kita tidak tahu apakah tangan kita bersih atau tidak. Jadi, hindari terus-terusan menyentuh mata, hidung, dan mulut.",
                R.mipmap.ic_no_touch_face_foreground),
        new Protocol("Terapkan Social Distancing", "Jangan berdekatan dengan orang lain, pastikan jarak minimal 1 meter.",
                R.drawable.ic_social_distancing),
        new Protocol("Tetap di rumah", "Jika tidak ada keperluan dan merasa sehat, tetaplah berada dirumah. Karena bisa saja kita menjadi penular atau kita yang tertular. Kerja, belajar, dan ibadah di rumah.",
                R.drawable.ic_stay_at_home)
    };
}
