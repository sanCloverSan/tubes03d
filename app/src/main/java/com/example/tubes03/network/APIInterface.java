package com.example.tubes03.network;

import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountryData;
import com.example.tubes03.model.SummaryData;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*
 *
 * Retrofit References:
 * -   https://androidpedia.net/en/tutorial/1132/retrofit2
 * -   https://www.youtube.com/watch?v=1FVoBM9t2Ik
 */

public interface APIInterface {
    /**
     * Mengambil semua data summary baik secara global maupun per negara
     *
     * @return objek SumarryData yang merupakan hasil konversi dari JSON ke objek kelas SummaryData
     */
    @GET("/summary")
    Call<SummaryData> getGlobalSummary();

    /**
     * Mengambil data perkembangan kasus berdasarkan slug negara
     *
     * @return objek List<CountryData> yang merupakan list yang berisi hasil konversi dari JSON ke objek kelas CountryData
     */
    @GET("/country/{countrySlug}")
    Call<List<CountryData>> getCountrySummary(@Query("countrySlug") String countrySlug);

    /**
     * Mengambil data perkembangan kasus berdasarkan slug negara
     *
     * @return objek List<CountryData> yang merupakan list yang berisi hasil konversi dari JSON ke objek kelas CountryData
     */
    @GET("total/dayone/country/{countrySlug}}/status/confirmed")
    Call<List<CountryData>> getCountryAllStatus(@Query("countrySlug") String countrySlug);

    /**
     * Mengambil data perkembangan kasus berdasarkan slug negara dari tanggal berdasarkan parameter from
     * sampai data sesuai dengan tanggal pada parameter to
     *
     * @return objek List<CountryData> yang merupakan list yang berisi hasil konversi dari JSON ke objek kelas CountryData
     */
    @GET("country/{countrySlug}?from={from}}&to={to}}")
    Call<List<CountryData>> getCountryAllStatus(@Query("countrySlug") String countrySlug,
                                                @Query("from") Date from,
                                                @Query("to") Date to);
    /**
     * Mengambil data nama, slug, dan ISO32 dari negara-negara yang tersedia pada API
     *
     * @return objek List<Country> yang merupakan list yang berisi hasil konversi dari JSON ke objek kelas Country
     */
    @GET("/countries")
    Call<List<Country>> getCountries();
}
