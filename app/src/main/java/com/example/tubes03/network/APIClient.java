package com.example.tubes03.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Reference:
 * - https://stackoverflow.com/questions/29380844/how-to-set-timeout-in-retrofit-library
 * */
public class APIClient {
    private static final String BASE_URL = "https://api.covid19api.com";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {

            //Set timeout dari request
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(50, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            //Buat objek retrofit yang akan menangani request berdasarkan method pada APIInterface yang dipanggil
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        }
        return retrofit;
    }

}
