package com.example.tubes03;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.example.tubes03.databinding.FragmentCountryBinding;
import com.example.tubes03.model.Country;
import com.example.tubes03.model.CountryData;
import com.example.tubes03.model.CountrySummary;
import com.example.tubes03.model.GlobalData;
import com.example.tubes03.model.SummaryData;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;

import static com.example.tubes03.model.UtilityFunction.formatNumberToString;

public class CountryFragment extends Fragment implements Contract.CountryView, OnItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    private final static int DROP_DOWN_PART = 0;
    private final static int PIE_CHART_PART = 1;
    private final static int CARD_VIEWS_PART = 2;
    private final static int LINE_CHART_PART = 3;

    private Context context;

    private Presenter presenter;
    private TextView tvAngkaKasusBaruTerkonfirmasi;
    private TextView tvAngkaTotalKematian;
    private TextView tvAngkaKematianBaru;
    private TextView tvAngkaTotalTerkonfirmasi;
    private TextView tvAngkaTotalKesembuhan;
    private TextView tvAngkaKesembuhanBaru;
    private TextView tvRequestMessage;
    private TextView tvRequestDate;
    private NestedScrollView nsv_country;
    private SwipeRefreshLayout swipeRefreshLayoutCountry;
    private LinearLayout llHomeLoadingScreen;
    private AnyChartView pieChartView;
    private SearchableSpinner spinner;
    private DropDownArrayAdapter adapter;
    private Pie pieChart;

    private View cachedView;

    public static CountryFragment newInstance(Context context, Presenter presenter) {
        CountryFragment fragment = new CountryFragment();
        fragment.context = context;
        fragment.presenter = presenter;
        return fragment;
    }

    public CountryFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("debug country frg", "onCreateVIew");
        if (this.cachedView == null) {
            super.onCreateView(inflater, container, savedInstanceState);
            FragmentCountryBinding binding = FragmentCountryBinding.inflate(getLayoutInflater());

            this.llHomeLoadingScreen = binding.llCountryLoadingScreen;
            this.tvRequestDate = binding.tvCountryRequestDate;
            this.nsv_country = binding.scrollViewCountry;
            this.tvAngkaKasusBaruTerkonfirmasi = binding.tvAngkaKasusBaruTerkonfirmasiC;
            this.tvAngkaTotalKematian = binding.tvAngkaTotalKematianC;
            this.tvAngkaKematianBaru = binding.tvAngkaKematianBaruC;
            this.tvAngkaTotalTerkonfirmasi = binding.tvAngkaTotalTerkonfirmasiC;
            this.tvAngkaTotalKesembuhan = binding.tvAngkaTotalKesembuhanC;
            this.tvAngkaKesembuhanBaru = binding.tvAngkaKesembuhanBaruC;
            this.swipeRefreshLayoutCountry = binding.swipeRefreshCountry;
            this.swipeRefreshLayoutCountry.setOnRefreshListener(this);
            this.pieChartView = binding.pieChartCountry;
            this.spinner = binding.dropdownMenu;

            ArrayList<Country> data = new ArrayList<>();

            //Ambil data ngara terakhir dipilih untuk dimasukkan ke dalam list untuk adapter spinner
            Country savedCountry = this.presenter.getSavedCountryFromPreference();
            data.add(savedCountry);

            this.presenter.setSelectedCountry(savedCountry);

            //Membuat adapter
            this.adapter = new DropDownArrayAdapter(getContext(), data);
            this.spinner.setAdapter(adapter);
            this.spinner.setOnItemSelectedListener(this);

            //Memilih negara terakhir yang dipilih agar muncul sebagai pilihan pada spinner
            this.spinner.setSelectedItem(1);


            //Melakukan request untuk mengambil list negara dari API
            this.getCountries(DROP_DOWN_PART);

            //Cached view digunakan, karena setiap berpindah fragment selalu dibuat ulang,
            this.cachedView = binding.getRoot();
        }

        return this.cachedView;
    }

    /**
     * Method untuk memunculkan loading screen
     */
    @Override
    public void showProgress() {
        //to be implemented
    }

    /**
     * Method untuk menghilangkan loading screen
     */
    @Override
    public void hideProgress() {
        //to be implemented
    }

    /**
     * Method untuk menampilkan error ketika request gagal
     *
     * @param throwable
     */
    @Override
    public void onResponseFailure(Throwable throwable) {
        //to be implemented

    }

    /**
     * Method untuk mengisi tampilan apabila request gagal atau tidak mengembalikan data
     */
    @Override
    public void setEmptyData() {
        //Ambil data ngara terakhir dipilih untuk dimasukkan ke dalam list untuk adapter spinner
        Country savedCountry = this.presenter.getSavedCountryFromPreference();
        //Mengambil data summary untuk data dari negara yang terakhir dipilih
        CountrySummary countrySummary = this.presenter.getCountrySummaryFromPreference();

        //Melakukan update pada tampilan
        this.setCardViewsPart(countrySummary);
        this.createPieChart(countrySummary);
    }

    @Override
    public void getCountries(int viewPart) {
        Handler handler = new Handler();
        Contract.View view = this;

        final Runnable r = new Runnable() {
            public void run() {
                presenter.getCountries(view, viewPart);
            }
        };

        handler.postDelayed(r, 500);
    }

    @Override
    public void getCountryAllStatus(String countrySlug) {
        //to be implemented
    }

    @Override
    public void getCountryAllStatus(String countrySlug, Date from, Date to) {
        //to be implemented
    }

    /**
     * Method untuk mengisi tampilan apabila request berhasil
     */
    @Override
    public void onResponseSuccess(List<CountryData> list, int viewPart) {
        //to be implemented
    }

    /**
     * Method untuk mengisi data list dropdown
     */
    @Override
    public void onResponseSuccess(List<Country> list) {
        //Sort list agar terurut alfabetis
        Collections.sort(list);

        //Update ulang isi dari adapter menggunakan list hasil request dari API
        this.adapter.setCountryList(list);

        //Mengambil negara yang terakhir dipilih dari shared preference
        Country savedCountry = this.presenter.getSavedCountryFromPreference();

        //Mengubah pilihan item terpilih pada spinner menjadi negara yang tersimpan di shared preference
        this.spinner.setSelectedItem(savedCountry);

        //Data summary dari  negara yang terakhir dipilih
        CountrySummary countrySummary = this.presenter.getCountrySummary(savedCountry.getSlug());

        //Update ulang CardViews dan pie chart
        this.setCardViewsPart(countrySummary);
        this.createPieChart(countrySummary);

        this.presenter.setSelectedCountry(savedCountry);
        //Update ulang isi dari sharedPreference. Ada kemungkinan data summary yang tersimpan di preference belum merupakan
        //data yang lebih update
        this.presenter.saveSelectedCountryToPreference(savedCountry, countrySummary);

    }


    /**
     * Method untuk melakukan manipulasi data hasil request ke area CardView
     *
     * @param countrySummary
     */
    public void setCardViewsPart(CountrySummary countrySummary) {
        if (countrySummary != null) {
            //Set tanggal data terakhir di-update
            String date = countrySummary.getDate();
            this.tvRequestDate.setText(date.split("T")[0]);

            //Set isi dari view card
            this.tvAngkaKasusBaruTerkonfirmasi.setText(formatNumberToString(countrySummary.getNewConfirmed()));
            this.tvAngkaTotalKematian.setText(formatNumberToString(countrySummary.getTotalDeaths()));
            this.tvAngkaKematianBaru.setText(formatNumberToString(countrySummary.getNewDeaths()));
            this.tvAngkaTotalTerkonfirmasi.setText(formatNumberToString(countrySummary.getTotalConfirmed()));
            this.tvAngkaTotalKesembuhan.setText(formatNumberToString(countrySummary.getTotalRecovered()));
            this.tvAngkaKesembuhanBaru.setText(formatNumberToString(countrySummary.getNewRecovered()));
        } else {
            this.setEmptyData();
        }
    }

    /**
     * Method untuk membuat Pie Chart data summary sebuah negara
     *
     * @param countrySummary Data summary negara hasil fetch dari API
     */
    public void createPieChart(CountrySummary countrySummary) {
        /*
         * Reference:
         * https://github.com/AnyChart/AnyChart-Android/blob/master/sample/src/main/java/com/anychart/sample/charts/PieChartActivity.java
         * https://www.anychart.com/technical-integrations/samples/android-charts/
         * https://github.com/AnyChart/AnyChart-Android
         * https://github.com/AnyChart/AnyChart-Android/issues/97
         */

        Log.d("debug pie chart", "start of line");
        if (countrySummary != null) {
//            FragmentCountryBinding binding = FragmentCountryBinding.inflate(getLayoutInflater());
//            this.pieChartView = binding.pieChartCountry;
//            APIlib.getInstance().setActiveAnyChartView(this.pieChartView);
//            Cartesian area = AnyChart.area();
//            area.removeAllSeries();
//            area.dispose();
//            this.pieChartView.clear();

            this.pieChartView.setVisibility(View.VISIBLE);
            boolean prevNull = false;
            if (this.pieChart == null) {
                this.pieChart = AnyChart.pie();
                prevNull = true;
            }


            //Data untuk membuat pie chart
            List<DataEntry> data = new ArrayList<>();
            data.add(new ValueDataEntry("Terkonfirmasi Baru", countrySummary.getNewConfirmed()));
            data.add(new ValueDataEntry("Total Terkonfirmasi", countrySummary.getTotalConfirmed()));
            data.add(new ValueDataEntry("Terpulihkan Baru", countrySummary.getNewRecovered()));
            data.add(new ValueDataEntry("Total Terpulihkan", countrySummary.getTotalRecovered()));
            data.add(new ValueDataEntry("Kematian Baru", countrySummary.getNewDeaths()));
            data.add(new ValueDataEntry("Total Kematian", countrySummary.getTotalDeaths()));

            //Set data untuk chart
            this.pieChart.data(data);

            //Explode chart agar mudah terbaca
            Number[] arrIndexExplode = {0, 1, 2, 3, 4, 5};
            this.pieChart.select(arrIndexExplode);

            Log.d("debug pie chart", this.presenter.getSelectedCountry().toString());

            //Set judul dari chart
            this.pieChart.title("Data Kasus Covid-19 di " + this.presenter.getSelectedCountry().getCountry());

            //Set agar angka persentase untuk tiap potongan berada di luar
            this.pieChart.labels().position("outside");

            //Set background chart mengikuti warna background fragment
            this.pieChart.background().fill("#16213E");

            //Set label, garis, dan legenda agar berwarna putih
            this.pieChart.stroke("2 #FFFFFF");
            this.pieChart.legend().fontColor("#FFFFFF");
            this.pieChart.labels().fontColor("#FFFFFF");

            //Mengatur posisi dari legenda
            this.pieChart.legend()
                    .position("center-bottom")
                    .itemsLayout(LegendLayout.HORIZONTAL)
                    .align(Align.CENTER);
            if (prevNull) {
                this.pieChartView.setChart(this.pieChart);
            }

            Log.d("debug pie chart", "end of line");
        }

    }


    @Override
    public void onItemSelected(View view, int position, long id) {
        //Memilih data country berdasarkan posisi
        Country selectedItem = this.adapter.getItem(position);
        if (selectedItem != null) {
            this.presenter.setSelectedCountry(selectedItem);
            CountrySummary countrySummary = this.presenter.getCountrySummary(selectedItem.getSlug());

            //Update tampilan
            this.setCardViewsPart(countrySummary);
            this.createPieChart(countrySummary);

            this.presenter.setSelectedCountry(selectedItem);
            //Update ulang isi dari sharedPreference. Ada kemungkinan data summary yang tersimpan di preference belum merupakan
            //data yang lebih update
            this.presenter.saveSelectedCountryToPreference(selectedItem, countrySummary);
        }
    }

    @Override
    public void onNothingSelected() {
        Toast.makeText(getContext(), "Nothing Selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        //Menutup jendela edit ketika berpindah fragment, sehingga tidak menimbulkan crash
        this.spinner.hideEdit();
    }

    @Override
    public void onRefresh() {
        Handler handler = new Handler();
        Contract.View view = this;

        final Runnable r = new Runnable() {
            public void run() {
                getCountries(DROP_DOWN_PART);
            }
        };

        handler.postDelayed(r, 1000);
        this.swipeRefreshLayoutCountry.setRefreshing(false);
    }
}
