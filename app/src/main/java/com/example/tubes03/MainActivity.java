package com.example.tubes03;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.tubes03.databinding.ActivityMainBinding;
import com.example.tubes03.model.Protocol;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ncapdevi.fragnav.FragNavController;

import java.util.Arrays;
import java.util.List;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private final static int NUMBER_OF_FRAGMENTS = 4;
    private final static int INDEX_HOME = 0;
    private final static int INDEX_COUNTRY = 1;
    private final static int INDEX_PROTOCOL = 2;
    private final static int INDEX_INFORMATION = 3;

    private BottomNavigationView bottomNavigationView;
    private FragNavController fragNavController;
    private ArrayList<Fragment> fragments;
    //Fragments
    private HomeFragment homeFragment;
    private CountryFragment countryFragment;
    private HealthProtocolFragment healthProtocolFragment;
    private InformationFragment informationFragment;

    //adapter
    private ProtocolAdapter adapter;
    private List<Protocol> protocolList;

    //Presenter
    private Presenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        this.presenter = new Presenter(this);
        this.bottomNavigationView = binding.bottomNavigation;
        this.bottomNavigationView.setOnNavigationItemSelectedListener(this);
        this.protocolList = Arrays.asList(ProtocolData.protocols);
        this.adapter = new ProtocolAdapter(this, this.presenter, this.protocolList);

        FrameLayout fragmentContainer = binding.fragmentContainer;
        this.fragNavController = new FragNavController(getSupportFragmentManager(), fragmentContainer.getId());

        //Create a list of fragments and pass fragments in
        this.fragments = new ArrayList<>();

        this.homeFragment = HomeFragment.newInstance(this, this.presenter);
        this.fragments.add(this.homeFragment);

        this.countryFragment = CountryFragment.newInstance(this, this.presenter);
        this.fragments.add(this.countryFragment);

        this.healthProtocolFragment = HealthProtocolFragment.newInstance(this.adapter);
        this.fragments.add(this.healthProtocolFragment);

        this.informationFragment = InformationFragment.newInstance();
        this.fragments.add(this.informationFragment);

        this.fragNavController.setRootFragments(this.fragments);
//
        //Set selected tab to TAB1 (index 0 / INDEX_HOME)
        this.fragNavController.initialize(FragNavController.TAB1, savedInstanceState);
//        this.loadFragment(this.homeFragment);
        setContentView(binding.getRoot());
    }

    /**
     * Method untuk memunculkan fragment pada parameter ke layar
     * @param fragment
     */
    private void loadFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Implementasi interface listener pada BottomNavigationView untuk berpindah fragment
     * menggunakan controller dari library FragNav
     * @param item item pada menu navbar yang ditekan
     * @return
     */
//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        Fragment fragment;
//        switch (item.getItemId()){
//            case R.id.action_home:
//                fragment =this.homeFragment;
//                loadFragment(fragment);
//                return true;
//            case R.id.action_country:
//                fragment = this.countryFragment;
//                loadFragment(fragment);
//                return true;
//            case R.id.action_protocol:
//                fragment = this.healthProtocolFragment;
//                loadFragment(fragment);
//                return true;
//            case R.id.action_info:
//                fragment = this.informationFragment;
//                loadFragment(fragment);
//                return true;
//        }
//        return false;
//
//    }


    //Reference: https://www.akshayrana.in/2020/07/bottom-navigation-bar-in-android.html

    /**
     * Implementasi interface listener pada BottomNavigationView untuk berpindah fragment
     * menggunakan controller dari library FragNav
     * @param item item pada menu navbar yang ditekan
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                this.fragNavController.switchTab(FragNavController.TAB1);
                return true;

            case R.id.action_country:
                this.fragNavController.switchTab(FragNavController.TAB2);
                return true;

            case R.id.action_protocol:
                this.fragNavController.switchTab(FragNavController.TAB3);
                return true;

            case R.id.action_info:
                this.fragNavController.switchTab(FragNavController.TAB4);
                return true;
        }
        return false;
    }
}