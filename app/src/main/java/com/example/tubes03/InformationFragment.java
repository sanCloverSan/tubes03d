package com.example.tubes03;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tubes03.databinding.FragmentCountryBinding;
import com.example.tubes03.databinding.FragmentInformationBinding;

public class InformationFragment extends Fragment {
    public static InformationFragment newInstance() {
        InformationFragment fragment = new InformationFragment();
        return fragment;
    }

    public InformationFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentInformationBinding binding = FragmentInformationBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

}
